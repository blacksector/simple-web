# Base Inamge
FROM node:alpine
WORKDIR /user/app

# instad dependencies
COPY ./package.json ./
RUN npm install
COPY ./ ./

# Default command
CMD ["npm","start"]